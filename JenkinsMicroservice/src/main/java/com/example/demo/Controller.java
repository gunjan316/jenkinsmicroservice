package com.example.demo;

import java.util.List;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {

	@Autowired
	StudentDAO sdao;

	@GetMapping("/getAll")
	public List<StudentBean> getAllCourses() {
		return sdao.getAll();
	}

	
}
