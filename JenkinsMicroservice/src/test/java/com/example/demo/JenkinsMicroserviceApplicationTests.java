package com.example.demo;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import junit.framework.Assert;

@RunWith(SpringRunner.class)
@SpringBootTest
public class JenkinsMicroserviceApplicationTests {

	@Autowired
	StudentDAO sdao;
	
	List<StudentBean> ls; 
	@Test
	public void checkStudents() {
		ls = new ArrayList<StudentBean>();
		ls.add(new StudentBean("Gunjan","12","Mumbai"));
		Assert.assertEquals(ls.get(0)+" ", sdao.getAll().get(0)+ " ");
		
	}

}
